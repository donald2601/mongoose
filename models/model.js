const mongoose = require('mongoose')

let schema = mongoose.Schema({
    name: String,
    age: Number,
    favoriteFoods: [String]
})

const model = mongoose.model('personne',schema)
module.exports= model;